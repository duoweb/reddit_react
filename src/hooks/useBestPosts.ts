import { useEffect, useState } from "react";
import axios from "axios";
import { useSelector } from "react-redux";
import { RootState } from "../store/rootReducer";

interface IUserData {
    posts: any[];
}

export function useBestPosts() {
    const [posts, setPosts] = useState<IUserData>({posts: []});
    const token = useSelector<RootState, string>(state => state.token);

    useEffect(() => {
        if (token && token.length > 0) {
            axios.get('https://oauth.reddit.com/best.json?limit=10&sr_detail=true', {
                headers: {
                    Authorization: `bearer ${token}`,
                }
            })
                .then((resp) => {
                    const postsData = resp.data.data.children;
                    setPosts({ posts: postsData });
                    // console.log(postsData);
                })
                .catch(console.log);
        }
    }, [token]);

    return [posts];
}