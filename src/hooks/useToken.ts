import { useDispatch } from "react-redux";
import { setToken } from "../store/token/actions";

console.log(444)
export function useToken() {
    const dispatch = useDispatch();

    if(typeof window !== "undefined" && window.__token__) {
        dispatch(setToken(window.__token__));
    }
}