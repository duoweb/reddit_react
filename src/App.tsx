import React from 'react';
import './main.global.css';
import { hot } from "react-hot-loader/root";
import { Layout } from "./shared/Layout";
import { Header } from "./shared/Header";
import { Content } from "./shared/Content";
import { CardsList } from "./shared/CardsList";
import { EColors, Text } from "./shared/Text";
import { Break } from "./shared/Break";
import { createStore } from "redux";
import { Provider, useDispatch } from "react-redux";
import { composeWithDevTools } from "@redux-devtools/extension";
import { rootReducer } from "./store/rootReducer";

const store = createStore(rootReducer, composeWithDevTools());

function AppComponent() {
    return (
        <Provider store={store}>
            <Layout>
                <Header />
                <Content>
                    <CardsList />
                    <br/>
                    <Text size={20} mobileSize={28} color={EColors.green} bold>Label1</Text>
                    <Break size={8} mobileSize={16} top/>
                    <Text size={20}>Label2</Text>
                    <Break size={8} mobileSize={16} top/>
                    <Text size={20} mobileSize={16}>Label3</Text>
                </Content>
            </Layout>
        </Provider>
    );
}

export const App = hot(() => <AppComponent/>);