import { assoc } from "../js/assoc";

export const generateRandomstring = () => Math.random().toString(36).substring(2, 15);

export const assignId = assoc('id', generateRandomstring());

export const generateId = <O extends object>(obj: O) => assignId(obj);