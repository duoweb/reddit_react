import React, {
  ChangeEvent,
  FormEvent,
  useEffect,
  useRef,
  useState,
} from "react";
import styles from "./commentform.css";

interface ICommentFormProps {
  authorName?: string,
  isOpenCommentForm?: boolean,
  value?: string,
  onChange?: (event: ChangeEvent<HTMLTextAreaElement>) => void,
  onSubmit?: (event: FormEvent) => void,
}

export default function CommentForm(props: ICommentFormProps) {
  const { authorName, isOpenCommentForm, value, onChange, onSubmit } = props;
  const textareaRef = useRef<HTMLTextAreaElement>(null);
  const buttonText = authorName ? "Ответить" : "Комментировать";
  const [isOpen, setIsOpen] = useState(
      isOpenCommentForm ? isOpenCommentForm : !authorName
  );

  useEffect(() => {
    if (isOpenCommentForm !== undefined) setIsOpen(isOpenCommentForm);
    if (isOpen) {
      textareaRef.current!.focus();
    }
  }, [isOpenCommentForm, isOpen]);

  return (
      <div className={`${styles.hidden}  ${isOpen ? styles.open : ""}`}>
        <form className={styles.form} onSubmit={onSubmit}>
        <textarea
            className={styles.input}
            value={`${value}`}
            onChange={onChange}
            ref={textareaRef}
        />
          <button type="submit" className={styles.button}>
            {buttonText}
          </button>
        </form>
      </div>
  );
}

