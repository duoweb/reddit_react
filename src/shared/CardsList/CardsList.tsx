import React from 'react';
import styles from './cardslist.css';
import { Card } from "./Card";
import { useSelector } from "react-redux";
import { RootState } from "../../store/rootReducer";

export function CardsList() {
    const posts  = useSelector<RootState, any>(state => state).postReducer.posts;

    if(!posts[0]) return (
        <div className={styles.cardsList}>
            Чтобы появились посты требуется авторизация...
        </div>
    );

    return (
        <ul className={styles.cardsList}>
            {posts && posts.map((post:any) => {
                const created = new Date(post.data.created).toLocaleString();
                return (
                    <Card
                        key={post.data.id}
                        title={post.data.title}
                        url={post.data.url}
                        author={post.data.author}
                        created={created}
                        imgSrc={post.data.sr_detail.header_img}
                        iconImg={post.data.sr_detail.icon_img}
                        authorSrc={'https://www.reddit.com/' + post.data.sr_detail.url}
                        postId={post.data.id}
                        postGroup={post.data.subreddit}
                    />
                )
            })}
        </ul>
    )
}
