import React from 'react';
import { BlockIcon, WarningIcon, CommentIcon, ShareIcon, SaveIcon } from "../../../icons";
import { EColors, Text } from "../../../Text";
import styles from './menuitemslist.css';

interface IMenuItemsListProps {
    postId: string;
}

export function MenuItemsList({postId}: IMenuItemsListProps) {
    return (
        <ul className={styles.menuItemsList}>
            <li className={`${styles.menuItem} ${styles.hiddenMobile}`}>
                <CommentIcon />
                <Text size={12} color={EColors.grey99}>Комментарии</Text>
            </li>

            <div className={`${styles.divider} ${styles.hiddenMobile}`}/>

            <li className={`${styles.menuItem} ${styles.hiddenMobile}`}>
                <ShareIcon />
                <Text size={12} color={EColors.grey99}>Поделиться</Text>
            </li>

            <div className={`${styles.divider} ${styles.hiddenMobile}`}/>

            <li className={styles.menuItem}>
                <BlockIcon/>
                <Text size={12} color={EColors.grey99}>Скрыть</Text>
            </li>

            <div className={styles.divider}/>

            <li className={`${styles.menuItem} ${styles.hiddenMobile}`}>
                <SaveIcon />
                <Text size={12} color={EColors.grey99}>Сохранить</Text>
            </li>

            <div className={`${styles.divider} ${styles.hiddenMobile}`}/>

            <li className={styles.menuItem}>
                <WarningIcon/>
                <Text size={12} color={EColors.grey99}>Пожаловаться</Text>
            </li>
        </ul>
    );
}
