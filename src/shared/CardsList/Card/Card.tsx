import React from 'react';
import styles from './card.css';
import { TextContent } from "./TextContent";
import { Preview } from "./Preview";
import { Menu } from "./Menu";
import { Controls } from "./Controls";

interface ICardProps {
    title: string,
    url: string,
    author: string,
    created: string,
    imgSrc: string,
    iconImg: string,
    authorSrc: string,
    postId: string,
    postGroup: string,
}

export function Card({ title, url, author, created, imgSrc, iconImg, authorSrc, postId, postGroup }: ICardProps) {
  return (
    <li className={styles.card}>
        <TextContent title={title} url={url} author={author} created={created} iconImg={iconImg} authorSrc={authorSrc} postId={postId} postGroup={postGroup}/>
        <Preview imgSrc={imgSrc}/>
        <Menu />
        <Controls />
    </li>
  );
}
