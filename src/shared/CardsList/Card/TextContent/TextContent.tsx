import React from 'react';
import styles from './textcontent.css';
import { Title } from "./Title";
import { UserLink } from "./Userlink";

interface ITextContentProps {
    title: string,
    url: string,
    author: string,
    created: string,
    iconImg: string,
    authorSrc: string,
    postId: string,
    postGroup: string,
}

export function TextContent({ title, url, author, created, iconImg, authorSrc, postId, postGroup }: ITextContentProps) {
    return (
        <div className={styles.textContent}>
            <div className={styles.metaData}>
                <UserLink author={author} iconImg={iconImg} authorSrc={authorSrc}/>
                <span className={styles.createdAt}>
                <span className={styles.publishedLabel}> опубликовано </span>
                    {created}
                </span>
            </div>
            <Title title={title} url={url} postId={postId} postGroup={postGroup}/>
        </div>
    );
}
