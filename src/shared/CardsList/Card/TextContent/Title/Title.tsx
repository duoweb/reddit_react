import React, { useState } from 'react';
import { Post } from '../../../../Post';
import styles from './title.css';

interface ITitleProps {
    title: string,
    url?: string,
    postId: string,
    postGroup: string,
}

export function Title({ title, url, postId, postGroup }: ITitleProps) {
    const [isModalOpened, setIsModalOpened] = useState(false);

    return (
        <h2 className={styles.title}>
            <a href='#' className={styles.postLink} onClick={() => {
                setIsModalOpened(true);
            }}>
                {title}
            </a>

            {isModalOpened && (
                <Post
                    postId={postId}
                    postGroup={postGroup}
                    title={title}
                    onClose={() => {setIsModalOpened(false)}}
                />
            )}
        </h2>
    );
}
