import React from 'react';
import styles from './userlink.css';
import { IconAnon, IconAnonSmall } from "../../../../icons";

interface IUserLinkProps {
    author: string;
    iconImg: string;
    authorSrc: string;
}

export function UserLink({ author, iconImg, authorSrc }: IUserLinkProps) {

    const defaultAvatar = '';

    return (
        <div className={styles.userLink}>
            {iconImg
                ? <img
                    className={styles.avatar}
                    src={iconImg}
                    alt="avatar"
                />
                : <IconAnonSmall />
            }


            <a href={authorSrc} className={styles.username}>{author}</a>
        </div>
    );
}
