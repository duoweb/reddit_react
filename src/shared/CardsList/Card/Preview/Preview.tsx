import React from 'react';
import styles from './preview.css';
import { IconAnon } from "../../../icons";

interface ITextProps {
    imgSrc?: string;
}

export function Preview({ imgSrc }: ITextProps) {
    return (
        <div className={styles.preview}>
            {imgSrc
                ? <img
                    className={styles.previewImg}
                    src={imgSrc}
                />
                : <img
                    className={styles.previewImg}
                    src={'https://avatars.mds.yandex.net/get-zen_doc/3753737/pub_601cf442d496d26713360fb0_601cf545a34fdb6f5839c9e1/scale_1200'}
                />
            }
        </div>
    );
}
