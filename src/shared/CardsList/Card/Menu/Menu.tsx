import React, { useEffect, useRef, useState } from 'react';
import styles from './menu.css';
import { MenuIcon } from "../../../icons";
import { EColors, Text } from "../../../Text";
import { MenuItemsList } from "../MenuItemsList";
import { DropdownPortal } from "../../../DropdownPortal";

export function Menu() {
    const ref = useRef<HTMLDivElement>(null);
    const [styleTop, setStyleTop] = useState<string>('');
    const [styleLeft, setStyleLeft] = useState<string>('');

    useEffect(() => {
        const top = (ref.current?.getBoundingClientRect().y)?.toString();
        const left = (ref.current?.getBoundingClientRect().x)?.toString();
        if(top) setStyleTop(top);
        if(left) setStyleLeft(left);
    }, []);


    return (
        <div className={styles.menu} ref={ref}>
            <DropdownPortal
                button={
                    <button className={styles.menuButton}>
                        <MenuIcon/>
                    </button>
                }

                top={styleTop}
                left={styleLeft}
            >
                <div className={styles.dropdown}>
                    <MenuItemsList postId='1234' />
                    <button className={styles.closeButton}>
                        <Text mobileSize={12} size={14} color={EColors.grey66}>
                            Закрыть
                        </Text>
                    </button>
                </div>
            </DropdownPortal>
        </div>
    );
}
