import React, {
    ChangeEvent,
    FormEvent,
} from "react";
import { useDispatch, useSelector } from "react-redux";
import CommentForm from "../CommentForm/CommentForm";
import { RootState } from "../../store/rootReducer";
import { updateComment } from "../../store/comment/actions";

interface ICommentFormProps {
    authorName?: string;
    isOpenCommentForm?: boolean;
}

export default function CommentFormContainer(props: ICommentFormProps) {
    const value = useSelector<RootState, any>(state => state).commentReducer.commentText;

    const dispatch = useDispatch();

    function handleChange(event: ChangeEvent<HTMLTextAreaElement>) {
        dispatch(updateComment(event.target.value));
    }

    function handleSubmit(event: FormEvent) {
        event.preventDefault();
    }

    return (
     <CommentForm
         value={value}
         onChange={handleChange}
         onSubmit={handleSubmit}
     />
    );
}

