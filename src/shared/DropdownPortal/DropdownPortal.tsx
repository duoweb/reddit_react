import React from 'react';
import styles from './dropdown.css';
import ReactDOM from "react-dom";

interface IDropdownProps {
    button: React.ReactNode;
    children: React.ReactNode;
    isOpen?: boolean;
    onOpen?: () => void;
    onClose?: () => void;
    top: string;
    left: string;
}

const NOOP = () => {};

export function DropdownPortal({button, children, isOpen, onClose = NOOP, onOpen = NOOP, top, left}: IDropdownProps) {
    const node = document.querySelector('#modal_root');
    if(!node) return null;
    const [isDropdownOpen, setIsDropdownOpen] = React.useState(isOpen);
    React.useEffect(() => setIsDropdownOpen(isOpen), [isOpen]);
    React.useEffect(() => isDropdownOpen ? onOpen() : onClose(), [isDropdownOpen]);

    const handleOpen = () => {
        if(isOpen === undefined) {
            setIsDropdownOpen(!isDropdownOpen)
        }
    }

    return ReactDOM.createPortal((
        <div className={styles.container} style={{top: `calc(${top}px + 18px)`, left: `${left}px`}}>
            <div onClick={handleOpen}>
                {button}
            </div>
            {isDropdownOpen && (
                <div className={styles.listContainer}>
                    <div className={styles.list} onClick={() => setIsDropdownOpen(false)}>
                        {children}
                    </div>
                </div>
            )}
        </div>
        ), node);
}
