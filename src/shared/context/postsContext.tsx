import React from "react";
import { useBestPosts } from "../../hooks/useBestPosts";

export interface IPostContextData {
    posts: any[];
}

export const postsContext = React.createContext<IPostContextData>({posts: []});

export function PostContextProvider({ children }: { children: React.ReactNode }) {
    const [posts] = useBestPosts();

    console.log(12312)

    return (
        <postsContext.Provider value={posts}>
            {children}
        </postsContext.Provider>
    )
}