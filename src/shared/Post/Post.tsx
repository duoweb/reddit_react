import React, { useEffect, useRef } from 'react';
import styles from './post.css';
import ReactDOM from "react-dom";
import { Comments } from "../Comments";
import CommentFormContainer from "../CommentFormContainer/CommentFormContainer";

interface IPost {
    title: string,
    onClose?: () => void,
    postId: string,
    postGroup: string,
}

export function Post(props: IPost) {
    const ref = useRef<HTMLDivElement>(null),
        { title, postId, postGroup } = props;

    useEffect(() => {
        function handleClick(event: MouseEvent) {
            if(event.target instanceof Node && !ref.current?.contains(event.target)) {
                props.onClose?.();
            }
        }

        document.addEventListener('click', handleClick);

        return () => {
            document.removeEventListener('click', handleClick);
        }
    }, []);

    const node = document.querySelector('#modal_root');
    if(!node) return null;

    return ReactDOM.createPortal((
        <div className={styles.modal} ref={ref}>
            <h2>{title}</h2>

            <div className={styles.content}>
                <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
            </div>
            <CommentFormContainer />
            <Comments postId={postId} postGroup={postGroup}/>
        </div>
        ), node);
}
