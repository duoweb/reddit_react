import React, { useEffect } from 'react';
import styles from './header.css';
import { SearchBlock } from "./SearchBlock";
import { ThreadTitle } from "./ThreadTitle";
import { SortBlock } from "./SortBlock";
import { useDispatch, useSelector } from "react-redux";
import { setToken } from "../../store/token/actions";
import { RootState } from "../../store/rootReducer";
import { setPosts } from "../../store/post/actions";
import axios from "axios";
import { setUserData } from "../../store/user/actions";


export function Header() {
    const dispatch = useDispatch();

    if(typeof window !== "undefined" && window.__token__) {
        const token = window.__token__;
        dispatch(setToken(token));

        if (token && token.length > 0) {
            axios.get('https://oauth.reddit.com/best.json?limit=10&sr_detail=true', {
                headers: {
                    Authorization: `bearer ${token}`,
                }
            })
                .then((resp) => {
                    const postsData = resp.data.data.children;
                    dispatch(setPosts(postsData));
                })
                .catch(console.log);

            axios({
                method: 'get',
                url: "https://oauth.reddit.com/api/v1/me.json",
                headers: { 'Authorization': `Bearer ${token}`  }
            }).then((resp) => {
                const userData = resp.data;
                dispatch(setUserData({ name: userData.name, iconImg: userData.snoovatar_img }));
            })
                .catch(console.log);
        }

    }



    return (
        <header className={styles.header}>
            <SearchBlock />
            <ThreadTitle />
            <SortBlock />
        </header>
    );
}
