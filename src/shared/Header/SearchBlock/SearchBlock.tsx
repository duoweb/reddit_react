import React, { useContext } from 'react';
import styles from './searchblock.css';
import { UserBlock } from "./UserBlock";
import { userContext } from "../../context/userContext";
import { useSelector } from "react-redux";
import { RootState } from "../../../store/rootReducer";
import { userReducer } from "../../../store/user/reducers";


export function SearchBlock() {
    const { iconImg, name } = useContext(userContext);
    const userData = useSelector<RootState, any>(state => state).userReducer.userData;

    return (
        <div className={styles.searchBlock}>
            <UserBlock avatarSrc={userData.iconImg} username={userData.name}/>
        </div>
    );
}
