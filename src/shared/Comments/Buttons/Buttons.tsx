import React, { createRef, useRef, useState } from "react";
import styles from "./buttons.css";
import { CommentIcon, ShareIcon, WarningIcon } from "../../icons";
import { EColors, Text } from "../../Text";
import CommentFormContainer from "../../CommentFormContainer/CommentFormContainer";

interface IButtonsProps {
  authorName?: string;
}

export function Buttons(props: IButtonsProps) {
  const { authorName } = props;
  const [isOpenCommentForm, setIsOpenCommentForm] = useState(false);

  const handleReply = () => {
    setIsOpenCommentForm(!isOpenCommentForm);
  };

  return (
    <div>
      <ul className={styles.menuItemsList}>
        <li onClick={handleReply}>
          <CommentIcon />
          <Text size={12} commentReply={true} color={EColors.grey99}>
            Ответить
          </Text>
        </li>
        <li>
          <ShareIcon />
          <Text size={12} color={EColors.grey99}>
            Поделиться
          </Text>
        </li>

        <li className={styles.menuItem}>
          <WarningIcon />
          <Text size={12} color={EColors.grey99}>
            Пожаловаться
          </Text>
        </li>
      </ul>
      <CommentFormContainer
        authorName={authorName}
        isOpenCommentForm={isOpenCommentForm}
      />
    </div>
  );
}
