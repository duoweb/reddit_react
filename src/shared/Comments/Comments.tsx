import React, { useEffect, useState } from 'react';
import styles from './comments.css';
import { Buttons } from "./Buttons";
import axios from "axios";

interface ICommentsProps {
    postId: string,
    postGroup: string
}

interface ICommentProps {
    data: any
}

export function Comments(props: ICommentsProps) {
    const { postId, postGroup } = props;
    const [comments, setComments] = useState([]);

    useEffect(() => {
        axios.get(`https://api.reddit.com/r/${postGroup}/comments/${postId}`)
            .then((resp) => {
                const commentsData = resp.data;

                setComments(commentsData);
            })
            .catch(console.log);
    }, []);


    return <div id={'comments'}>
        {comments && comments.map((comment: ICommentProps, index) => {
            return (comment.data.children && comment.data.children.map((mainCommentData: any, index: number) => {
                if(mainCommentData.data.body) {
                    mainCommentData.data.isOpen = false;

                    const created = new Date(mainCommentData.data.created_utc).toLocaleString();

                    return <div key={index}>
                        <div className={styles.headerContainer}>
                            <div><img width={30}
                                      src="https://i.redd.it/snoovatar/avatars/232b35e6-a04d-477f-9a72-3683d38f2e26.png"
                                      alt="avatar ico"/></div>
                            <div>{mainCommentData.data.author}</div>
                            <div>{created}</div>
                            <div>{mainCommentData.data.subreddit}</div>
                        </div>

                        <div className={styles.contentContainer}>
                            <div>
                                {mainCommentData.data.body}
                            </div>

                            <div className={styles.buttonsContainer}>
                                <Buttons authorName={mainCommentData.data.author}></Buttons>
                            </div>

                            {mainCommentData.data.replies && mainCommentData.data.replies.data.children.map((secondCommentData: any, index: number) => {
                                if(secondCommentData.data.body) {
                                    secondCommentData.data.isOpen = false;
                                    return <div key={index}>
                                        <div className={styles.headerContainer}>
                                            <div><img width={30}
                                                      src="https://i.redd.it/snoovatar/avatars/232b35e6-a04d-477f-9a72-3683d38f2e26.png"
                                                      alt="avatar ico"/></div>
                                            <div>{secondCommentData.data.author}</div>
                                            <div>{created}</div>
                                            <div>{secondCommentData.data.subreddit}</div>
                                        </div>

                                        <div className={styles.contentContainer}>
                                            <div>
                                                {secondCommentData.data.body}
                                            </div>

                                            <div className={styles.buttonsContainer}>
                                                <Buttons authorName={secondCommentData.data.author}></Buttons>
                                            </div>
                                        </div>
                                    </div>
                                }

                            })}
                        </div>
                    </div>
                }
            }))
        })}
    </div>

}
