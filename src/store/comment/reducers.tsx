import { Reducer } from "redux";
import { UPDATE_COMMENT } from "./actions";

export type RootState = {
    commentText: string,
}
const initialState = {
    commentText: '',
}

export const commentReducer: Reducer<RootState> = (state = initialState, action) => {
    switch (action.type) {
        case UPDATE_COMMENT:
            return {
                ...state,
                commentText: action.text,
            };
        default:
            return state;
    }
}