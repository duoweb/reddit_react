import { ActionCreator, AnyAction } from "redux";

export const SET_TOKEN = 'SET_TOKEN';
export const setToken: ActionCreator<AnyAction> = (token) => ({
    type: SET_TOKEN,
    token,
})
