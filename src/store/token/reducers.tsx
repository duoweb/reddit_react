import { Reducer } from "redux";
import { SET_TOKEN } from "./actions";

export type RootState = {
    token: string,
}
const initialState = {
    token: '',
}

export const tokenReducer: Reducer<RootState> = (state = initialState, action) => {
    switch (action.type) {
        case SET_TOKEN:
            return {
                ...state,
                token: action.token,
            };
        default:
            return state;
    }
}