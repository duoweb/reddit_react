import { Reducer } from "redux";
import { SET_USER_DATA } from "./actions";

export type RootState = {
    userData:{
        name:string,
        iconImg:string
    }
}

const initialState = {
    userData:{
        name:'',
        iconImg:''
    }
}

export const userReducer: Reducer<RootState> = (state = initialState, action) => {

    switch (action.type) {
        case SET_USER_DATA:
            return {
                ...state,
                userData: action.userData,
            };
        default:
            return state;
    }
}