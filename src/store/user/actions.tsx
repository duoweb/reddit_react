import { ActionCreator, AnyAction } from "redux";

export const SET_USER_DATA = 'SET_USER_DATA';
export const setUserData: ActionCreator<AnyAction> = (userData) => ({
    type: SET_USER_DATA,
    userData,
})
