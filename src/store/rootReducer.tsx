import { combineReducers } from "redux";
import { tokenReducer } from "./token/reducers";
import { postReducer } from "./post/reducers";
import { userReducer } from "./user/reducers";
import { commentReducer } from "./comment/reducers";

export type RootState = {
    token: string,
    posts: any[],
    commentText: string,
    userData: { name:string, iconImg:string}
}

export const rootReducer = combineReducers({
    tokenReducer,
    postReducer,
    userReducer,
    commentReducer,
})
