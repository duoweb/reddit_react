import { Reducer } from "redux";
import { UPDATE_POST } from "./actions";

export type RootState = {
    posts: any[],
}
const initialState = {
    posts: [],
}

export const postReducer: Reducer<RootState> = (state = initialState, action) => {
    switch (action.type) {
        case UPDATE_POST:
            return {
                ...state,
                posts: action.posts,
            };
        default:
            return state;
    }
}