import { ActionCreator, AnyAction } from "redux";

export const UPDATE_POST = 'UPDATE_POST';
export const setPosts: ActionCreator<AnyAction> = (posts) => ({
    type: UPDATE_POST,
    posts,
})
