//1. Работа с простыми типами
type MyConcatType = {
    (str1: string, str2: string) : string;
}

const concat: MyConcatType = function fnConcat(str1, str2) {
    return str1 + str2;
}

concat('Hello ', 'World') // -> Hello World;

//2. Работа с интерфейсами
interface MyObj {
    howIDoIt: string;
    simeArray: [string, number];
}

interface IMyHomeTask {
    howIDoIt: string;
    simeArray: [string, string, number];
    withData: Array<MyObj>;
}

const MyHomeTask: IMyHomeTask = {
    howIDoIt: "I Do It Wel",
    simeArray: ["string one", "string two", 42],
    withData: [{ howIDoIt: "I Do It Wel", simeArray: ["string one", 23] }],
}

//3. Типизация функций, используя Generic
interface IMyArray<T> {
    [N: number]: T;

    map(fn: (el: T) => T): T[]

    reduce(fn: (sum: T, current: T, index: number, array: T[]) => T, initialValue: T): T;

}

const initialValue = 0;
[1,2,3].reduce((accumulator, value) => accumulator + value, initialValue); // -> 6

const myArray: IMyArray<number> = [1,2,3];
myArray.map((f) => f + 1);
myArray.reduce((sum, current) => sum + current, initialValue);

//4. Работа с MappedTypes

interface IHomeTask {
    data: string;
    numbericData: number;
    date: Date;
    externalData: {
        basis: number;
        value: string;
    }
}

type MyPartial<T> = {
    [N in keyof T]?: T[N] extends object ? MyPartial<T[N]> : T[N]
}


const HomeTask: MyPartial<IHomeTask> = {
    externalData: {
        value: 'win'
    }
}

